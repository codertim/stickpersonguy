package timforce.stick;

import android.app.Activity;
import android.os.Bundle;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;


public class StickPerson extends Activity implements OnClickListener {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        View playGameButton = findViewById(R.id.start_button);
        playGameButton.setOnClickListener(this);
    }
    
    public void onClick(View v) {
    	switch(v.getId()) {
    	case R.id.start_button:
    		Intent i = new Intent(this, PlayGame.class);
    		startActivity(i);
    		break;
    	}
    }
}