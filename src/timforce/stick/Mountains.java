package timforce.stick;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.Log;

public class Mountains {
    /** Called when the activity is first created. */
	final static String TAG = "Mountains";
	int maxX = 300;  // screen width - helps know when to reverse direction
	int groundFromTop = 100;   // distance of ground from top of screen
	static final Paint mountainPaint = new Paint();
	Path mtnPath1 = new Path();
	Path mtnPath2 = new Path();

	
	public Mountains(int maxX, int groundFromTop) {
		this.maxX = maxX;
		this.groundFromTop = groundFromTop;
		this.mountainPaint.setStyle(Style.FILL);
		// this.mountainPaint.setColor(Color.GREEN);
		this.mountainPaint.setARGB(255, 34, 139, 34);   // forest green
	}
	
		
	public void drawSelf(Canvas canvas) {
		drawMountains(canvas);
	}
	
	
	// TODO: remove cPaint from class if we can
	private void drawMountains(Canvas canvas) {
		mtnPath1.reset();   // TODO: get rid of this if not needed
		mtnPath1.moveTo(-40,  groundFromTop);
		mtnPath1.lineTo(100, groundFromTop-50);
		mtnPath1.lineTo(200, groundFromTop);
		mtnPath1.lineTo(-40,  groundFromTop);
		
		mtnPath2.reset();   // TODO: get rid of this if not needed
		mtnPath2.moveTo(120,  groundFromTop);
		mtnPath2.lineTo(290, groundFromTop-50);
		mtnPath2.lineTo(350, groundFromTop);
		mtnPath2.lineTo(120,  groundFromTop);
		
		canvas.drawPath(mtnPath1, mountainPaint);
		canvas.drawPath(mtnPath2, mountainPaint);
	}
	
}


