package timforce.stick;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.Log;

public class Clouds {
    /** Called when the activity is first created. */
	final static String TAG = "Person";
	static final int bodyHeight  = 30;
	static final int bodyWidth   = 1;
	static final int radius      = 50;
	int centerX   = 100;
	int aboveGround   = 50;
	int directionX = 1;   // +1 for right, -1 for left  - TODO: try enumeration
	int defaultMovement = 1;  // number of pixels to move clouds per frame
	int maxX = 300;  // screen width - helps know when to reverse direction
	int groundFromTop = 1;   // distance of ground from top of screen
	static final Paint cloudPaint = new Paint();
	String cloudType = "hump1";
	
	
	public Clouds(int maxX, int groundFromTop) {
		this.maxX = maxX;
		this.groundFromTop = groundFromTop;
		this.cloudPaint.setColor(Color.YELLOW);
		this.cloudPaint.setAntiAlias(true);
		// this.cloudPaint.setAlpha(127);
	}
	
	
	// move in default direction (no special state/circumstances)
	public void moveDefault() {
		centerX += (defaultMovement * directionX);
		// Log.d(TAG, "#moveDefault - centerx = " + centerX);
		
		// check boundaries
		if(centerX > (maxX + radius)) {
			// prevent falling off right edge
			centerX = -radius;
			setRandomColor();
			setRandomCloudType();
		} 
	}
	
	
	private void setRandomColor() {
		double randNum = Math.random();
		
		if(randNum > 0.8) {
			cloudPaint.setColor(Color.MAGENTA);
		} else if(randNum > 0.6) {
			cloudPaint.setColor(Color.YELLOW);
		} else if(randNum > 0.4) {
			cloudPaint.setColor(Color.GREEN);
		} else if(randNum > 0.2) {
			cloudPaint.setColor(Color.CYAN);
		} else {
			cloudPaint.setColor(Color.RED);
		}		
	}
	
	
	public void drawSelf(Canvas canvas, Paint cPaint) {
		drawCloud(canvas, cPaint);
	}
	
	
	// TODO: remove cPaint from class if we can
	private void drawCloud(Canvas canvas, Paint cPaint) {
		// Log.d(TAG, "#drawCloud - groundFromTop=" + groundFromTop + "     aboveGround=" + aboveGround);

		// first draw ellipse that all clouds have in common
		RectF cloudOval = new RectF(centerX - radius, groundFromTop - aboveGround - (radius/3), centerX + radius, groundFromTop - aboveGround + (radius/3));
		canvas.drawOval(cloudOval, cloudPaint);		

		// now determine if any hump should be drawn on ellipse
		if(cloudType.equals("HUMP1")) {
			canvas.drawCircle((centerX-5), (groundFromTop - aboveGround - 15), 15, cloudPaint);
		}
	}
	
	
	private void setRandomCloudType() {
		double rand = Math.random();
		if(rand > 0.50) {
			cloudType = "HUMP0";   // ellipse w/no humps
			this.cloudPaint.setAlpha(220);
		} else {
			cloudType = "HUMP1";   // ellipse w/1 hump
			this.cloudPaint.setAlpha(255);
		}
	}
	
}


