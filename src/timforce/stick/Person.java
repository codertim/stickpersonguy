package timforce.stick;


import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;

public class Person {
    /** Called when the activity is first created. */
	static final int bodyHeight  = 20;
	static final int bodyWidth   = 1;
	static final int radius      = 5;
	static final Path headPath      = new Path();
	static final Path bodyPath      = new Path();
	static final Path leftHandPath  = new Path();
	static final Path rightHandPath = new Path();
	static final Path leftFootPath  = new Path();
	static final Path rightFootPath = new Path();
	final static String TAG = "Person";
	int centerX   = 100;
	int centerY    = 1;
	int leftHandOffsetX  = -10;
	int rightHandOffsetX = +10;
	int leftHandOffsetY  = -10;
	int rightHandOffsetY = -10;	
	int leftFootOffsetX  = -10;
	int rightFootOffsetX = +10;
	int leftFootOffsetY  = +10;
	int rightFootOffsetY = +10;
	boolean isJumpState  = false;
	int jumpStateCounter =  0;
	int groundFromTop    = 1;
	int directionX = 1;   // +1 for right, -1 for left  - TODO: try enumeration
	int defaultMovement = 3;  // number of pixels to move person per frame
	int maxX = 300;  // screen width - helps know when to reverse direction
	
	
	// public Person(int center, int centerY) {
	// 	this.centerX = center;
	// 	this.centerY = centerY;
	// }
	
	public Person(int maxX, int groundFromTop) {
		this.maxX = maxX;
		this.centerY = groundFromTop;
	}
	
	
	public void moveViaUserInput(double eventPosX) {
		Log.d(TAG, "#moveViaUserInput - Starting ...");
		if(eventPosX > centerX) {
			this.centerX += defaultMovement;
		} else if(eventPosX < centerX){
			this.centerX -= defaultMovement;
		}
	}
	
	
	// move in default direction (no special state/circumstances)
	public void moveDefault() {
		// centerX += (defaultMovement * directionX);
		// Log.d(TAG, "#moveDefault - centerx = " + centerX);
		
		// check boundaries
		if(centerX > (maxX - 10)) {
			// prevent falling off right edge
			reverseDirectionX();
			centerX -= radius;
		} else if(centerX < radius) {
			// prevent falling off left edge
			reverseDirectionX();
			centerX += radius;
		}
	}
	
	
	private void reverseDirectionX() {
		directionX *= -1;
	}
	
	
	public void drawSelf(Canvas canvas, Paint cPaint) {
		updateCounters();
		drawHead(canvas, cPaint);
		drawBody(canvas, cPaint);
		drawLeftHand(canvas, cPaint);
		drawRightHand(canvas, cPaint);
		drawLeftFoot(canvas, cPaint);
		drawRightFoot(canvas, cPaint);
	}
	
	
	private void drawLeftHand(Canvas canvas, Paint cPaint) {
		// leftHandPath.reset();  // TODO: re-evaluate if path needed
		canvas.drawLine(centerX, getRelativeCenterY(), (centerX+leftHandOffsetX), (getRelativeCenterY()+leftHandOffsetY), cPaint);
	}
	
	
	private void drawRightHand(Canvas canvas, Paint cPaint) {
		// rightHandPath.reset();  // TODO: re-evaluate if path needed
		canvas.drawLine(centerX, getRelativeCenterY(), (centerX+rightHandOffsetX), (getRelativeCenterY()+rightHandOffsetY), cPaint);	
	}
	
	
	private void drawLeftFoot(Canvas canvas, Paint cPaint) {
		// leftFootPath.reset();  // TODO: re-evaluate if path needed
		// Log.d(TAG, "#drawLeftFoot - getRelativeCenterY()=" + getRelativeCenterY());
		canvas.drawLine(centerX, (getRelativeCenterY()+(bodyHeight/2)), (centerX+leftFootOffsetX), (getRelativeCenterY()+(bodyHeight/2)+leftFootOffsetY), cPaint);
	}
	
	
	private void drawRightFoot(Canvas canvas, Paint cPaint) {
		// rightFootPath.reset();  // TODO: re-evaluate if path needed
		canvas.drawLine(centerX, (getRelativeCenterY()+(bodyHeight/2)), (centerX+rightFootOffsetX), (getRelativeCenterY()+(bodyHeight/2)+rightFootOffsetY), cPaint);	
	}
	
	private void drawHead(Canvas canvas, Paint cPaint) {
		headPath.reset();   // clear previous circle
		headPath.addCircle(centerX, getHeadTop(), radius, Path.Direction.CW);
		canvas.drawPath(headPath, cPaint);
	}
	
	private void drawBody(Canvas canvas, Paint cPaint) {
		bodyPath.reset();   // clear previous rect
		bodyPath.addRect(getBodyLeft(), getBodyTop(), getBodyRight(), (getBodyTop()+bodyHeight), Path.Direction.CW);
		canvas.drawPath(bodyPath, cPaint);
	}
	
	// TODO: move this to static initializer
	private int getBodyRight() {
		int bodyWidthRight = bodyWidth/2;
		if(bodyWidthRight == 0) {
			bodyWidthRight = 1;   // or else body does not show up
		}
		int bodyRight = centerX + bodyWidthRight;
		// Log.d(TAG, "#getBodyRight - Return - value=" + bodyRight);
		return bodyRight;
	}
	
	private int getBodyLeft() {		
		int bodyLeft = centerX - (bodyWidth/2);
		// Log.d(TAG, "#getBodyLeft - Return - value=" + bodyLeft);
		return(bodyLeft);
	}
	
	private int getBodyTop() {
		return(getRelativeCenterY() - (bodyHeight/2));
	}
	
	private int getHeadTop() {
		return(getRelativeCenterY() - bodyHeight/2 - radius);
	}
	
	public void jump() {
		Log.d(TAG, "#jump - Starting ...");
		isJumpState = true;
		jumpStateCounter = 20;
	}
	
	private int getRelativeCenterY() {
		int relativeCenterY = 0;
		
		if(isJumpState) {
			if(jumpStateCounter > 17) {
				relativeCenterY = centerY - 10;
			} else if (jumpStateCounter > 14) {
				relativeCenterY = centerY - 20;
			} else if (jumpStateCounter > 11) {
				relativeCenterY = centerY - 30;
			} else if (jumpStateCounter > 8) {
				relativeCenterY = centerY - 20;
			} else if (jumpStateCounter > 4) {
				relativeCenterY = centerY - 10;
			} else {
				relativeCenterY = centerY;
			}
		} else {
			relativeCenterY = centerY;
		}
		// Log.d(TAG, "#getRelativeCenterY - relativeCenterY=" + relativeCenterY);
		return relativeCenterY;
	}
	
	
	private void updateCounters() {
		Log.d(TAG, "#updateCounters - isJumpState=" + isJumpState + "   jumpStateCounter = " + jumpStateCounter);
		if(isJumpState) {
			jumpStateCounter--;
		}
		if(jumpStateCounter < 1) {
			isJumpState = false;
		}
	}
}