package timforce.stick;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


public class PlayGame extends Activity {
    /** Called when the activity is first created. */
	final static String TAG = "PlayGame";
	static final int screenWidth = 300;
	static final int groundFromTop = 200;
	static final int gameBackgroundColor = Color.BLACK;
	static final Person   mainPerson = new Person(screenWidth, groundFromTop);
	static final Clouds   clouds     = new Clouds(screenWidth, groundFromTop);
	static final Mountains mountains = new Mountains(screenWidth, groundFromTop);
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.playgame);
        setContentView(new GraphicsView(this));
    }
    
    static public class GraphicsView extends View {
    	public GraphicsView(Context context) {
    		super(context);
    	}
    	
    	@Override
    	public boolean onTouchEvent(MotionEvent event) {
    		Log.d(TAG, "#onTouchevent - event message=" + event.toString());
    		int action = event.getAction();
    		switch(action) {
    		case (MotionEvent.ACTION_DOWN):
    			Log.d(TAG, "case = MotionEvent.ACTION_DOWN");
    			mainPerson.jump();
    			mainPerson.moveViaUserInput(event.getX());
    			break;
    		// case (MotionEvent.ACTION_MOVE):
    		// 	Log.d(TAG, "case = MotionEvent.ACTION_MOVE");
    		// 	mainPerson.jump();
    		}
    		return super.onTouchEvent(event);
    	}
    	
    	@Override
    	protected void onDraw(Canvas canvas) {
    		Paint cPaint = new Paint();
    		cPaint.setColor(Color.CYAN);
    		// canvas.drawPath(head,  cPaint);
    		startLoop(canvas, cPaint);
    	}
    	
    	private void startLoop(final Canvas canvas, final Paint cPaint) {
    		int i = 0;
    		while(i < 5) {
    			i++;
    			// Log.d(TAG, "#startLoop - counter = " + i);
    			canvas.drawColor(gameBackgroundColor);
    			moveObjects();
    			drawObjects(canvas, cPaint);
    			invalidate();   // really, really, really draw on screen
    			SystemClock.sleep(50);   // 10 works well
    		}
    	}
    	
    	
    	private void moveObjects() {
    		mainPerson.moveDefault();
    		clouds.moveDefault();
    	}
    	
    	
    	private void drawObjects(Canvas canvas, Paint cPaint) {
			mountains.drawSelf(canvas);
			mainPerson.drawSelf(canvas, cPaint);
			clouds.drawSelf(canvas, cPaint);
    	}
    	

    }
}